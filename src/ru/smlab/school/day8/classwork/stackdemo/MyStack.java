package ru.smlab.school.day8.classwork.stackdemo;

public class MyStack {

    String[] elements = new String[10]; //{null, null, null, null, null, null}
    int stackTop = -1;

    //добавляет новый элемент
    public void push(String newElement){
        if (stackTop > elements.length - 1) {
            System.out.println("Stack overflow");
            return;
        }
        elements[++stackTop] = newElement;
    }

    //забирает из стека верхний элемент
    public String pop() {
        if (stackTop < 0) {
            System.out.println("Stack contains no elements");
            return null;
        }
        return elements[stackTop--];
    }

}
