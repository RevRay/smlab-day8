package ru.smlab.school.day8.classwork.stackdemo;

public class StackDemo {
    public static void main(String[] args) {
        MyStack stack = new MyStack();

        System.out.println(stack.pop());
        stack.push("работа Иванова");
        stack.push("работа Петрова");
        stack.push("работа Федякина");
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        System.out.println(stack.elements.length);
    }
}
