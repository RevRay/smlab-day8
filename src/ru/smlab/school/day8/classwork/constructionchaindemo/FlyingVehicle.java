package ru.smlab.school.day8.classwork.constructionchaindemo;

public class FlyingVehicle extends Vehicle {
    double flySpeed;

    public FlyingVehicle(int passengers, double flySpeed) {
        super(passengers);
        this.flySpeed = flySpeed;
    }

    public void fly() {
        System.out.println("Взлет осуществлен");
    }

    public void land() {
        System.out.println("Посадка осуществлена");
    }
}
