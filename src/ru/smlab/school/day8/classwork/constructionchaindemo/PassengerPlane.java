package ru.smlab.school.day8.classwork.constructionchaindemo;

public class PassengerPlane extends FlyingVehicle {
    double degreesOutside;

    public PassengerPlane(int passengers, double flySpeed, double degreesOutside) {
        super(passengers, flySpeed);
        this.degreesOutside = degreesOutside;
    }

    public void captainAnnouncement() {
        System.out.println(String.format("Уважаемые пассажиры! На нашем борту в настоящий момент находится '%d'. " +
                "Температура за бортом составляет '%f' градусов.", passengers, degreesOutside));
    }

}
