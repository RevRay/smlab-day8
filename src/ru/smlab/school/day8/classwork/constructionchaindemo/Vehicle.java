package ru.smlab.school.day8.classwork.constructionchaindemo;

public class Vehicle {
    int passengers;

    public Vehicle(int passengers) {
        System.out.println("Vehicle constructor called");
        this.passengers = passengers;
    }
}
