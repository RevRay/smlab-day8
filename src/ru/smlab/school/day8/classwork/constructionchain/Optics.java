package ru.smlab.school.day8.classwork.constructionchain;

public class Optics {
    int lensCount; //количество линз
    double magnification; //увеличительная способность линзы

    public Optics(int lensCount, double magnification){
        this.lensCount = lensCount;
        this.magnification = magnification;
        System.out.println("Создал экземпляр класса Optics ");
    }
}
