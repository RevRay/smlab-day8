package ru.smlab.school.day8.classwork.constructionchain;

public class Binocular extends Optics {
    String model;
    double distance;

    public double getTotalMagnification() {
        return lensCount * magnification;
    }

    public Binocular() {
        super(10, 2.5);
        System.out.println("Создал экземпляр класса Binocular");
    }
}
