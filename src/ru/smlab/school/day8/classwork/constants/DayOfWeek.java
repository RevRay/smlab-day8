package ru.smlab.school.day8.classwork.constants;

public enum DayOfWeek {
    MONDAY(false, 8), //final DayOfWeek MONDAY
    TUESDAY(false, 8),
    WEDNESDAY(false, 8),
    THURSDAY(false, 8),
    FRIDAY(false, 7),
    SATURDAY(true, 0),
    SUNDAY(true, 0);

    DayOfWeek(boolean isHoliday, int workLoad) {
        this.isHoliday = isHoliday;
        this.workLoad = workLoad;
    }

    boolean isHoliday;
    int workLoad;
}
