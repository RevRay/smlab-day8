package ru.smlab.school.day8.classwork.constants;

public enum Directions {
    NORTH(0),
    EAST(90),
    SOUTH(180),
    WEST(270);

    static int compensation = 360 / (Directions.values().length * 2);

    private Directions(int degree){
        this.degree = degree;
    }

    int degree;

    public static Directions getDirectionByInputDegree(int degree) {
        degree = (degree / 90) % 4; //0,1,2,3
        //TODO out of 0, 360 and negatives
        for (Directions d : Directions.values()) {
            if (degree < d.degree + Directions.compensation) {
                System.out.println(d);
                return d;
            }
        }
        return null;
    }
}
