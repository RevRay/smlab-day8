package ru.smlab.school.day8.classwork.constants;

import java.util.Scanner;

public class CompassDemo {
    static final int MAXIMUM_DEGREES = 360; // 4
    static final String[] directions = {"North", "East", "South", "West"}; //TODO
    static final int compensation = MAXIMUM_DEGREES / (directions.length * 2);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputDegree = sc.nextInt(); // 0 - 360

        Directions result = Directions.getDirectionByInputDegree(inputDegree);
    }




    public static void main2(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputDegree = sc.nextInt(); // 0 - 360

        String result = "Unknown";

        int resultIndex = ((inputDegree + compensation) / 90) % 4;   //0,1,2,3

        System.out.println(directions[resultIndex]);
    }




    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputDegree = sc.nextInt(); // 0 - 360

        String result = "Unknown";

        if (inputDegree < MAXIMUM_DEGREES / 4 - 45) {
            result = directions[0];
        } else if (inputDegree < MAXIMUM_DEGREES / 2 - 45) {
            result = directions[1];
        } else if (inputDegree < MAXIMUM_DEGREES * 0.75 - 45) {
            result = directions[2];
        } else if (inputDegree < 360 - 45) {
            result = directions[3];
        }


        System.out.println(result);
    }
}
