package ru.smlab.school.day8.classwork.constants;

import java.util.Arrays;

public class EnumDemo {
    public static void main(String[] args) {
        DayOfWeek[] allDays = DayOfWeek.values();

        for(DayOfWeek day : allDays) {
            System.out.println(day.toString() + day.workLoad + day.isHoliday );
        }

        DayOfWeek d = DayOfWeek.MONDAY;
        d.workLoad = 12;

    }
}
