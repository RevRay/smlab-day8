package ru.smlab.school.day8.classwork.finaldemo;

public class B extends A{
    @Override
    public void printSomething() {
        System.out.println("from B");
    }


    public static void main(String[] args) {
        B b = new B();
        b.printSomething();
    }
}
